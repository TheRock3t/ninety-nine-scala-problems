package main.scala

object Lists {

  /**
   * p01 - Find the last element of a list
   */
  def last[A](list: List[A]): A =
    if (list.length == 1) list.head else last(list.tail)

  /**
   * p02 - Find the last but one element of a list
   */
  def penultimate[A](list: List[A]): A =
    if (list.length == 2) list.head else penultimate(list.tail)

  /**
   * p03 - Find the Kth element of a list
   */
  def nth[A](n: Int, list: List[A]): A =
    if (n == 1) list.head else nth(n - 1, list.tail)

  /**
   * p04 - Find the number of elements of a list
   */
  def length[A](list: List[A]): Int =
    if (list.isEmpty) 0 else 1 + length(list.tail)

  /**
   * p05 - Reverse a list
   */
  // Simple recursive
  def reverseRecursive[A](ls: List[A]): List[A] = ls match {
    case Nil => Nil
    case h :: tail => reverseRecursive(tail) ::: List(h)
  }

  // Tail recursive.
  def reverseTailRecursive[A](ls: List[A]): List[A] = {
    def reverseR(acc: List[A], list: List[A]): List[A] = list match {
      case Nil => acc
      case h :: tail => reverseR(h :: acc, tail)
    }
    reverseR(Nil, ls)
  }

  /**
   * p06 - Find out whether a list is a palindrome
   * isPalindrome(List(9, 8, 3, 8, 9))	>	true
   * isPalindrome(List(9, 8, 8, 9))	>	true
   * isPalindrome(List(9))	>	true
   */
  def isPalindrome[A](list: List[A]): Boolean = {
    if (list.length <= 1) true
    else (list.head == list.takeRight(1).head) && isPalindrome(list.tail.take(list.tail.length - 1))
  }

  /**
   * p07 - Flatten a nested list structure
   * flatten(List(List(2, 2), 2, List(3, List(5, 6))))	>	List(2, 2, 2, 3, 5, 6)
   */
  def flatten(list: List[Any]): List[Any] = list flatMap {
    case sublist: List[Any] => flatten(sublist)
    case element: Any => List(element)
  }

  /**
   * p08 - Eliminate consecutive duplicates of list elements
   * compress(List('a', 'a', 'f', 'f', 'f', 'a', 'd', 'd'))	>	List('a', 'f', 'a', 'd')
   */
  def compress[A](list: List[A]): List[A] = list match {
    case Nil => Nil
    case x: List[A] => x.head :: compress(x.dropWhile(_ == x.head))
  }

  /**
   * p09 - Pack consecutive duplicates of list elements into sublists
   * pack(List('a', 'a', 'f', 'f', 'f', 'a', 'd', 'd'))	>	List(List('a', 'a'), List('f', 'f', 'f'), List('a'), List('d', 'd'))
   */
  def pack(list: List[Any]): List[List[Any]] = list match {
    case Nil => Nil
    case x: List[Any] => x.takeWhile(_ == x.head) :: pack(x.dropWhile(_ == x.head))
  }
  
  /**
   * p10 - Run-length encoding of a list
   * superPack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))	>	List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
   */
  def superPack(list: List[Any]): List[(Int, Any)] = pack(list) map { x=> (x.length, x.head)}
}