# What is this repository for? #

This repo contains a project with the solutions for the collection of ninety-nine logic problems. Efficiency is important, but logical clarity is even more crucial.

### Structure ###
The project contains a *.scala file for each chapter (List, Arithmetic, Logic and Codes etc.) and for the more complex one unit tests will be in place. Currently the Lists problems are being solved.

### Contribution guidelines ###
If one finds better solutions, please feel free to email the owner of the repo. In this way we could review our code and find the optimal solutions.

### Who do I talk to? ###
Repo owner